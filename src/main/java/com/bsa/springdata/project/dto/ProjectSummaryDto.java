package com.bsa.springdata.project.dto;



//// TODO: Use this interface when you make a projection from native query.
////  If you don't use native query replace this interface with a simple POJO
//public interface ProjectSummaryDto {
//    String getName();
//    long getTeamsNumber();
//    long getDevelopersNumber();
//    String getTechnologies();
//}

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ProjectSummaryDto{
    private String name;
    long teamsNumber;
    long developersNumber;
    String technologies;
}
