package com.bsa.springdata.project;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {


    @Query(value = "select t.project from Team t where t.technology.name = :technology order by t.users.size desc")
    List<Project> findTopProjectsByTechnology(String technology, Pageable page);

    @Query("select t.project from Team t order by t.project.teams.size desc, t.users.size desc, t.name desc")
    List<Project> findBiggestProject(Pageable page);

    @Query("select count(distinct u.team.project) from User u inner join Role r on r member of u.roles")
    int getProjectsWithRole(String role);

    Optional<Project> findByName(String name);

    @Query("select t.technology.name from Team t where t.project.name = :name")
    List<String> getProjectTechnologies(String name);

    @Query("select count(u) from User u where u.team.project.name = :name")
    long countProjectsUsers(String name);

    @Query("select pr.teams.size from Project pr where pr.name = :name")
    long countProjectTeams(String name);

}
