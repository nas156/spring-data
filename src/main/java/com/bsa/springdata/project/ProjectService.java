package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.TeamRepository;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.team.TechnologyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TechnologyRepository technologyRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        // TODO: Use single query to load data. Sort by number of developers in a project
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        return projectRepository.findTopProjectsByTechnology(technology, PageRequest.of(0,5)).stream()
                .map(ProjectDto::fromEntity).collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        // TODO: Use single query to load data. Sort by teams, developers, project name
        //  Hint: in order to limit the query you can either use native query with limit or Pageable interface
        return projectRepository.findBiggestProject(PageRequest.of(0,1)).stream().map(ProjectDto::fromEntity).findFirst();
    }

    public List<ProjectSummaryDto> getSummary() {
        // TODO: Try to use native query and projection first. If it fails try to make as few queries as possible
        return projectRepository.findAll().stream().map(Project::getName).map(this::summaryWithName).collect(Collectors.toList());
    }

    private ProjectSummaryDto summaryWithName(String name){
        return ProjectSummaryDto.builder()
                .name(name)
                .technologies(projectRepository.getProjectTechnologies(name).stream().map(Object::toString)
                        .collect(Collectors.joining(",")))
                .developersNumber(projectRepository.countProjectsUsers(name))
                .teamsNumber(projectRepository.countProjectTeams(name))
                .build();
    }

    public int getCountWithRole(String role) {
        // TODO: Use a single query
        return projectRepository.getProjectsWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        // TODO: Use common JPARepository methods. Build entities in memory and then persist them
        var newTechnology = Technology.builder()
                .name(createProjectRequest.getTech())
                .link(createProjectRequest.getTechLink())
                .build();
        var newTeam = Team.builder()
                .area(createProjectRequest.getTeamArea())
                .name(createProjectRequest.getTeamName())
                .room(createProjectRequest.getTeamRoom())
                .technology(newTechnology)
                .build();
        var newProject = Project.builder()
                .name(createProjectRequest.getProjectName())
                .description(createProjectRequest.getProjectDescription())
                .teams(Collections.singletonList(newTeam))
                .build();



        technologyRepository.save(newTechnology);
        projectRepository.save(newProject);
        return projectRepository.findByName(createProjectRequest.getProjectName()).get().getId();
    }
}
