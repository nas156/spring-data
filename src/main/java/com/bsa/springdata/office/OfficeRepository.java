package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Table;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query(value = "select distinct u.office from User u where u.team.technology.name = :technology")
    List<Office> findByTechnology(String technology);

    @Transactional
    @Modifying
    @Query("update Office o set o.address = :newAddress where o.address = :oldAddress and o.users.size>0") //where u.team.technology is not null
    void updateOfficeAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String Address);
}
