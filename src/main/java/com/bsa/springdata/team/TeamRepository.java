package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.beans.Transient;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Query("select t from Team t where t.name = :name")
    Optional<TeamRepository> findByName(String name);

    int countByTechnologyName(String newTechnology);

    @Transactional
    @Modifying
    @Query("update Technology t set t.name = :newTechnologyName where t.name = :oldTechnologyName and t in " +
            "(select team.technology from Team team where team.users.size < :devsNum)")
    void selectAllTechnologiesByTeamUsersNumAndTechnologyNameName(int devsNum, String oldTechnologyName, String newTechnologyName);

    @Transactional
    @Modifying
    @Query(nativeQuery = true,
        value = "update teams set name = concat(teams.name, '_', pr.name," +
                "'_', tc.name) from " +
                "((teams te inner join projects pr on te.project_id = pr.id) " +
                "inner join technologies tc on te.technology_id = tc.id) " +
                "where teams.name = ?")
    void normalizeName(String oldName);

}
