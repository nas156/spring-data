package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    @Query("select u from User u where lower(u.lastName) like lower(concat('%', ?1,'%')) order by u.lastName asc")
    List<User> findAllByLastNameContainingIgnoreCaseOrderByLastNameAsc(String lastName, Pageable page);

    List<User> findAllByOffice_CityOrderByLastNameAsc(String city);

    List<User> findAllByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);


    List<User> findAllByOffice_CityAndTeam_Room(String city, String room, Pageable page);

    @Modifying
    @Query(value = "delete from User u where u.experience < :experience")
    int deleteAllByExperienceLessThan(int experience);
}
